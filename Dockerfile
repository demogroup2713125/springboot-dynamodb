FROM openjdk:11
EXPOSE 8080
ADD target/springboot-dynamodb-curd.jar springboot-dynamodb-curd.jar
ENTRYPOINT ["java","-jar","/springboot-dynamodb-curd.jar"]